package com.test.a;

public class A {
    static {
        System.out.println("A1");
    }

    {
        System.out.println("Object Create");
    }

    public static void main(String[] args) {
        System.out.println("MAIN");
        A a = new A();
        a.m1();

        A a1 = new A();
        a1.m1();

        System.out.println(a == a1);
        System.out.println(a.equals(a1));
    }


    private void m1() {
        System.out.println("Method 1");
    }
}
