package com.test.tasks.warmup1;

public class SleepIn {
    static boolean sleepIn(boolean weekday, boolean vacation) {
        return !weekday || vacation;
    }

    public static void main(String[] args) {

        if (sleepIn(true, false))
            System.out.println("спим");
        else
            System.out.println("проснулись");
    }

}
